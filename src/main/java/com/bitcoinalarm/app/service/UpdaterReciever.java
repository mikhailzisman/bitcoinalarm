package com.bitcoinalarm.app.service;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 16.04.13
 * Time: 16:11
 * To change this template use File | Settings | File Templates.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import com.bitcoinalarm.app.ExchangeAPI.MtGoxClient;
import com.bitcoinalarm.app.Utils.AlarmShow;
import com.bitcoinalarm.app.Utils.Notifications;
import com.bitcoinalarm.app.database.HelperFactory;
import com.bitcoinalarm.app.domens.Alarm;
import com.bitcoinalarm.app.domens.MtGoxResult;
import com.bitcoinalarm.app.interfaces.IDataListener;

import java.sql.SQLException;
import java.util.ArrayList;

//http://stackoverflow.com/questions/4459058/repeatUpdaterReciever-manager-example
public class UpdaterReciever extends BroadcastReceiver implements IDataListener {

    private static final String TAG = "UpdaterReciever";
    MtGoxClient mt = new MtGoxClient(this, "4214d6f4-7a53-400a-9ba8-c32aaffb4b8e", "0kN8/a9A1F+xd/YiN9o5KjyqjBbjLGIvL7pSnRRFi4xgC+SzP0paqICtqwfX43PoTP2lhXLC5xWBGe+89KlE/A==");
    private AlarmShow alarmUI = null;
    private Alarm curAlarm = null;
    private int requestsCount = 0;
    private int resultsCount = 0;

    @Override
    public void onReceive(Context context, Intent intent) {


        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();

        if (alarmUI == null)
            alarmUI = new AlarmShow(context);

        try {
            ArrayList<Integer> takenCurrencyIDs = new ArrayList<Integer>();

            for (Alarm alarm : HelperFactory.getHelper().getAlarmDAO().getAllAlarms()) {
                if (!alarm.isEnable()) continue;
                if (takenCurrencyIDs.indexOf(Integer.valueOf(alarm.getCurrencyID())) == -1)
                    takenCurrencyIDs.add(alarm.getCurrencyID());
                else continue;
                curAlarm = alarm;
                requestsCount++;
                runRequests(context, curAlarm);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        wl.release();
    }

    private void runRequests(Context context, Alarm alarm) {
        Notifications.getInstance().setState(context, Notifications.Mode.Processing);
        mt.startGetRate(alarm, 1, System.currentTimeMillis());
    }

    @Override
    public void updateData(ArrayList<MtGoxResult> results) {
        //save results to database
        try {
            for (MtGoxResult result : results) {
                result.setAlarmID(curAlarm.getId()); // Not correct!!!!!!!!!!
                HelperFactory.getHelper().getMtGoxResultDAO().create(result);
            }

            resultsCount++;

            if (requestsCount == resultsCount)
                for (Alarm alarm : HelperFactory.getHelper().getAlarmDAO().getAllAlarms()) {
                    if (!alarm.isEnable()) continue;
                    if (alarmUI != null)
                        alarmUI.showAlarmIfEventHappens(alarm);


                }


            //clearResultsDatabase(curAlarm);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void clearResultsDatabase(Alarm alarm) throws SQLException {
        HelperFactory.getHelper()
                .getMtGoxResultDAO()
                .clearTableUntilPeriodTimeByAlarmID(MtGoxClient.CurrencyArray[alarm.getCurrencyID()],
                                                    MtGoxClient.TypeResultArray[alarm.getResultTypeID()],
                                                    alarm.getPeriodTimeInSecond());
    }
}