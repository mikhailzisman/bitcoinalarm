package com.bitcoinalarm.app.service;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import com.bitcoinalarm.app.Utils.Utils;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 07.05.13
 * Time: 11:57
 * To change this template use File | Settings | File Templates.
 */
public class ExchangesServiceControl {

    public static void startOrStopServiceIfNeed(Context context) {
        if ((!isExchangesServiceRunning(context))) {
            if (Utils.isOnline(context))
                context.startService(new Intent(context, ExchangesService.class));
        } else if (!Utils.isOnline(context)) {
            context.stopService(new Intent(context, ExchangesService.class));
        }
    }

    private static boolean isExchangesServiceRunning(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (ExchangesService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
