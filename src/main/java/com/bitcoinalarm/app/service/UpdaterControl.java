package com.bitcoinalarm.app.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import com.bitcoinalarm.app.Utils.Notifications;
import com.bitcoinalarm.app.database.HelperFactory;
import com.bitcoinalarm.app.domens.Alarm;
import com.bitcoinalarm.app.domens.Utils;
import com.bitcoinalarm.app.interfaces.IListenerRepositoryChanged;

import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 16.04.13
 * Time: 23:00
 * To change this template use File | Settings | File Templates.
 */
public class UpdaterControl implements IListenerRepositoryChanged {

    final static UpdaterControl instance = new UpdaterControl();
    private static final String TAG = "UpdaterControl";
    private SharedPreferences mSharedPreference;

    public static UpdaterControl getInstanse() {
        return instance;
    }

    public void startUpdater(Context context) {
        onAlarmUpdated(context);
    }

    private void setRefreshProcess(Context context, long RefreshRateInSeconds) {

        enableNotification(context);
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, UpdaterReciever.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + RefreshRateInSeconds * 1000, RefreshRateInSeconds * 1000, pi); // Millisec * Second * Minute
    }

    private void enableNotification(Context context) {
        if (existsActiveAlarm(context)) Notifications.getInstance().setState(context, Notifications.Mode.Working);
                else Notifications.getInstance().setState(context, Notifications.Mode.Canceled);
    }

    private boolean existsActiveAlarm(Context context) {
        try {
            List<Alarm> alarmList = HelperFactory.getHelper().getAlarmDAO().getAllAlarms();
            for (Alarm alarm:alarmList){
                if (alarm.isEnable()) return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return false;
    }

    private void cancelRefreshProcess(Context context) {
        Intent intent = new Intent(context, UpdaterReciever.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        //  | PendingIntent.FLAG_ONE_SHOT
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    @Override
    public void onAlarmStopped(Context context) {
        cancelRefreshProcess(context);
    }

    @Override
    public void onAlarmStarted(Context context) {

        setRefreshProcess(context, calculateRefreshRate(context));
    }

    @Override
    public void onAlarmUpdated(Context context) {
        cancelRefreshProcess(context);
        setRefreshProcess(context, calculateRefreshRate(context));
    }

    private long calculateRefreshRate(Context context) {

        long refreshRateInSeconds = Utils.getRefreshRateFromSharedPreferenceInSeconds(context);

        Alarm alarmWithMinPeriod = getAlarmWithMinPeriodFromDatabase();
        if ((alarmWithMinPeriod != null)&&(alarmWithMinPeriod.isEnable()))
            if (refreshRateInSeconds > alarmWithMinPeriod.getPeriodTimeInSecond()) {
                refreshRateInSeconds = getAlarmWithMinPeriodFromDatabase().getPeriodTimeInSecond();

                //if we found refresh rate less than current in SettingsFragmet, we should to put it there.
                Utils.saveSharedPreferenceFromAlarm(context,getAlarmWithMinPeriodFromDatabase());
            }

        return refreshRateInSeconds;

    }

    private Alarm getAlarmWithMinPeriodFromDatabase() {
        Alarm alarmResult = null;
        long refreshRateInSeconds = Long.MAX_VALUE;
        try {
            for (Alarm alarm : HelperFactory.getHelper().getAlarmDAO().getAllAlarms()) {
                //only for goes up and goes down alarms
                if (alarm.getReasonAlertID() >= 2)
                    if (alarm.getPeriodTimeInSecond() < refreshRateInSeconds) {
                        refreshRateInSeconds = alarm.getPeriodTimeInSecond();
                        alarmResult = alarm;
                    }
            }

            if (alarmResult == null)
                alarmResult = HelperFactory.getHelper().getAlarmDAO().getAlarmWithMinPeriodTime();
            return alarmResult;
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return null;
    }
}
