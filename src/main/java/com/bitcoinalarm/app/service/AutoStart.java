package com.bitcoinalarm.app.service;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 16.04.13
 * Time: 17:43
 * To change this template use File | Settings | File Templates.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.bitcoinalarm.app.service.ExchangesService;
import com.bitcoinalarm.app.service.UpdaterReciever;

public class AutoStart extends BroadcastReceiver {
    UpdaterReciever repeatUpdaterReciever = new UpdaterReciever();

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            context.startService(new Intent(context, ExchangesService.class));
        }
    }
}
