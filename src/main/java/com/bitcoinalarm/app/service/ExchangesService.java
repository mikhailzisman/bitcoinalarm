package com.bitcoinalarm.app.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.bitcoinalarm.app.Utils.Utils;
import com.bitcoinalarm.app.interfaces.IUpdaterControl;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 06.04.13
 * Time: 17:31
 * To change this template use File | Settings | File Templates.
 */
public class
        ExchangesService extends Service {

    static final String TAG = "ExchangesService";

    IUpdaterControl updaterControl;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (Utils.isOnline(this))
            UpdaterControl.getInstanse().startUpdater(getApplicationContext());

        return super.onStartCommand(intent, flags, startId);
    }


}
