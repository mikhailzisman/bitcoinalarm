package com.bitcoinalarm.app.database;

import com.bitcoinalarm.app.domens.License;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 06.05.13
 * Time: 12:35
 * To change this template use File | Settings | File Templates.
 */

public class LicenseDAO extends BaseDaoImpl<License, Integer> {
    public LicenseDAO(ConnectionSource connectionSource, Class<License> LicenseClass) throws java.sql.SQLException {
        super(connectionSource, LicenseClass);
    }

    public List<License> getAllLicense() throws java.sql.SQLException {
        return this.queryForAll();
    }

    public License getLastActiveLicense(int licenseType) throws SQLException {
        QueryBuilder<License, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq("licenseType",licenseType);
        queryBuilder.orderBy("timeOfRegistration",false);
        queryBuilder.limit(1);

        PreparedQuery<License> preparedQuery = queryBuilder.prepare();
        License license = queryForFirst(preparedQuery);
        return license;
    }

}


