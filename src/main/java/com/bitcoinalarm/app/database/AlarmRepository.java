package com.bitcoinalarm.app.database;

import com.bitcoinalarm.app.service.UpdaterControl;
import com.bitcoinalarm.app.domens.Alarm;
import com.bitcoinalarm.app.domens.EntityState;
import com.bitcoinalarm.app.interfaces.IAlarmRepository;
import com.bitcoinalarm.app.interfaces.IListModelData;
import com.bitcoinalarm.app.interfaces.IListenerRepositoryChanged;

import java.sql.SQLException;
import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 05.04.13
 * Time: 2:27
 * To change this template use File | Settings | File Templates.
 */
public class AlarmRepository implements IAlarmRepository {


    private final static IListModelData<Alarm> alarmListModel = new AlarmListModel();
    private static IAlarmRepository instance = new AlarmRepository();
    private IListenerRepositoryChanged listenerRepositoryChanged = UpdaterControl.getInstanse();


    public AlarmRepository() {

    }

    public static IAlarmRepository getInstance() {
        return instance;
    }

    public static AlarmListModel getAlarmListModel() {
        return (AlarmListModel) alarmListModel;
    }

    @Override
    public synchronized void load() {
        try {
            IListModelData<Alarm> res = (IListModelData<Alarm>) alarmListModel;
            res.getItemList().clear();
            res.getDeletedItemList().clear();
            List<Alarm> alarmList = HelperFactory.getHelper().getAlarmDAO().queryForAll();
            for (Alarm alarm : alarmList) {
                res.getItemList().add(alarm);
                alarm.setState(EntityState.NOT_CHANGED);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized void save() {

        IListModelData<Alarm> model = (IListModelData<Alarm>) alarmListModel;

        Exception ex;
        try {
            for (Alarm alarm : model.getItemList()) {
                if (alarm.getState() == null) continue;
                switch (alarm.getState()) {
                    case CHANGED:
                        HelperFactory.getHelper().getAlarmDAO().update(alarm);
                        listenerRepositoryChanged.onAlarmUpdated(HelperFactory.Context);
                        break;
                    case ADDED:
                        HelperFactory.getHelper().getAlarmDAO().create(alarm);
                        listenerRepositoryChanged.onAlarmStarted(HelperFactory.Context);
                        break;
                    case DELETED:
                        ex = new Exception("Не должно быть сущности со статусом DELETED в основной коллекции");
                        break;
                }

                alarm.setState(EntityState.NOT_CHANGED);

            }


            for (Alarm alarm : model.getDeletedItemList()) {
                if (alarm.getState() == null) continue;

                switch (alarm.getState()) {
                    case CHANGED:
                        ex = new Exception("Не должно быть сущности со статусом CHANGED в удаляемой коллекции");
                        break;
                    case ADDED:
                        ex = new Exception("Не должно быть сущности со статусом ADDED в удаляемой коллекции");
                        break;
                    case DELETED:
                        HelperFactory.getHelper().getAlarmDAO().delete(alarm);
                        listenerRepositoryChanged.onAlarmStopped(HelperFactory.Context);
                        break;
                }
            }


            model.getDeletedItemList().clear();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
