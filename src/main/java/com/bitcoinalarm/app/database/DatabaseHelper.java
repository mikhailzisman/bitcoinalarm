package com.bitcoinalarm.app.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.bitcoinalarm.app.domens.Alarm;
import com.bitcoinalarm.app.domens.License;
import com.bitcoinalarm.app.domens.MtGoxResult;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;


public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String TAG = DatabaseHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "myappname.db";

    private static final int DATABASE_VERSION = 31;


    private AlarmDAO alarmDAO = null;
    private LicenseDAO licenseDAO = null;
    private MtGoxResultDAO mtGoxResultsDAO = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Alarm.class);
            TableUtils.createTable(connectionSource, MtGoxResult.class);
            TableUtils.createTable(connectionSource, License.class);
        } catch (java.sql.SQLException e) {
            Log.e(TAG, "error creating DB " + DATABASE_NAME);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer,
                          int newVer) {
        try {

            TableUtils.dropTable(connectionSource, Alarm.class, true);
            TableUtils.dropTable(connectionSource, MtGoxResult.class, true);
            TableUtils.dropTable(connectionSource, License.class, true);
            onCreate(db, connectionSource);
        } catch (java.sql.SQLException e) {
            Log.e(TAG, "error upgrading db " + DATABASE_NAME + "from ver " + oldVer);
            throw new RuntimeException(e);
        }
    }

    public AlarmDAO getAlarmDAO() throws java.sql.SQLException {
        if (alarmDAO == null) {
            alarmDAO = new AlarmDAO(getConnectionSource(), Alarm.class);
        }
        return alarmDAO;
    }

    public LicenseDAO getLicenseDAO() throws java.sql.SQLException {
        if (licenseDAO == null) {
            licenseDAO = new LicenseDAO(getConnectionSource(), License.class);
        }
        return licenseDAO;
    }


    public MtGoxResultDAO getMtGoxResultDAO() throws java.sql.SQLException {
        if (mtGoxResultsDAO == null) {
            mtGoxResultsDAO = new MtGoxResultDAO(getConnectionSource(), MtGoxResult.class);
        }
        return mtGoxResultsDAO;
    }


    @Override
    public void close() {
        super.close();
        alarmDAO = null;
        mtGoxResultsDAO = null;
        licenseDAO = null;
    }
}