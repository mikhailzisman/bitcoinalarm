package com.bitcoinalarm.app.database;

import com.bitcoinalarm.app.ExchangeAPI.MtGoxClient;
import com.bitcoinalarm.app.domens.Alarm;
import com.bitcoinalarm.app.domens.MtGoxResult;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 08.04.13
 * Time: 3:46
 * To change this template use File | Settings | File Templates.
 */
public class MtGoxResultDAO extends BaseDaoImpl<MtGoxResult, Integer> {

    public MtGoxResultDAO(ConnectionSource connectionSource, Class<MtGoxResult> mtGoxResultClass) throws SQLException {
        super(connectionSource, mtGoxResultClass);
    }

    public void add(MtGoxResult mtGoxResult) throws SQLException {
        this.create(mtGoxResult);
    }

    public List<MtGoxResult> getAllRoles() throws SQLException {
        return this.queryForAll();
    }


    //direction
    //rise the price - true
    //drop below price - false
    public boolean ifIntersectPriceEdgeInPeriod(String currency,String resultType, Double edgePrice, long periodTimeInMillisec, boolean direction) throws SQLException {
        QueryBuilder<MtGoxResult, Integer> queryBuilder = queryBuilder();

        MtGoxResult lastResultByCurrency = getLastResultByCurrency(currency, resultType);

        if (lastResultByCurrency==null) return false;

        long endTime = lastResultByCurrency.getTimeNow();

        if (direction)
            queryBuilder.where().eq("currency", currency)
                    .and().eq("resultType",resultType)
                    .and().gt("value", edgePrice)
                    .and().between("timeNow", endTime - periodTimeInMillisec * 1000, endTime);
        else
            queryBuilder.where().eq("currency", currency)
                    .and().eq("resultType",resultType)
                    .and().lt("value", edgePrice)
                    .and().between("timeNow", endTime - periodTimeInMillisec * 1000, endTime);


        PreparedQuery<MtGoxResult> preparedQuery = queryBuilder.prepare();
        List<MtGoxResult> result = query(preparedQuery);
        return !result.isEmpty();
    }


    //direction
    //true - is growing in period
    //false - is falling in period
    public boolean ifGrowingPriceForPeriod(String currency,String resultType, long periodTimeInMillisec, double edgePrice, boolean direction) throws SQLException {

        long endTime = getLastResultByCurrency(currency, resultType).getTimeNow();

        MtGoxResult lastResultInPeriod = getLastResultByCurrency(currency, resultType);
        MtGoxResult firstResultInPeriod = getResultFirstValueInPeriod(currency,resultType,periodTimeInMillisec);

        if (((lastResultInPeriod.getValue()-firstResultInPeriod.getValue()>=edgePrice)
            & direction)) return true;

        if (((lastResultInPeriod.getValue()-firstResultInPeriod.getValue()<=-edgePrice)
                & !direction)) return true;


        return false;

    }

    public MtGoxResult getLastResultByCurrency(String currency, String resultType) throws SQLException {
        QueryBuilder<MtGoxResult, Integer> queryBuilder = queryBuilder();
        queryBuilder.where()
              .eq("currency", currency).and()
              .eq("resultType",resultType);
        queryBuilder.orderBy("timeNow", false);
        queryBuilder.limit(1);

        PreparedQuery<MtGoxResult> preparedQuery = queryBuilder.prepare();
        MtGoxResult mtGoxResult = queryForFirst(preparedQuery);
        return mtGoxResult;
    }

    public MtGoxResult getLastResultByAlarm(Alarm alarm) {

        try {
            return  getLastResultByCurrency(
                    MtGoxClient.CurrencyArray[alarm.getCurrencyID()],
                    MtGoxClient.TypeResultArray[alarm.getResultTypeID()]);
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }




    public MtGoxResult getResultFirstValueInPeriod(String currency, String resultType, long periodTimeInMillisec) throws SQLException {

        QueryBuilder<MtGoxResult, Integer> queryBuilder = queryBuilder();


        queryBuilder.where()
                .eq("currency", currency).and()
                .eq("resultType",resultType)
                .and().between("timeNow", getLastResultByCurrency(currency,resultType).getTimeNow() - periodTimeInMillisec * 1000-1000,
                                          getLastResultByCurrency(currency,resultType).getTimeNow());
        queryBuilder.orderBy("timeNow", true);
        queryBuilder.limit(1);

        PreparedQuery<MtGoxResult> preparedQuery = queryBuilder.prepare();
        MtGoxResult firstMtGoxResultFromPeriod = queryForFirst(preparedQuery);
        return firstMtGoxResultFromPeriod;
    }


    public void clearTableUntilPeriodTimeByAlarmID(String currency, String resultType, long periodTimeInSec) throws SQLException {

        long endTime = getLastResultByCurrency(currency,resultType).getTimeNow();
        QueryBuilder<MtGoxResult, Integer> queryBuilder = queryBuilder();

        queryBuilder.where().lt("timeNow", endTime - periodTimeInSec * 1000 * 1000);

        PreparedQuery<MtGoxResult> preparedQuery = queryBuilder.prepare();
        this.delete(query(preparedQuery));
    }
}
