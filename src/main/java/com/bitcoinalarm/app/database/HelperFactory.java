package com.bitcoinalarm.app.database;

import android.content.Context;
import com.j256.ormlite.android.apptools.OpenHelperManager;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 10.04.13
 * Time: 3:10
 * To change this template use File | Settings | File Templates.
 */
public class HelperFactory {

    private static DatabaseHelper databaseHelper;

    public static Context Context;

    public static DatabaseHelper getHelper() {
        return databaseHelper;
    }

    public static void setHelper(Context context) {
        if (databaseHelper == null)
            databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);

        Context = context;

    }

    public static void releaseHelper() {
        OpenHelperManager.releaseHelper();
        databaseHelper = null;
    }
}