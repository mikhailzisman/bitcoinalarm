package com.bitcoinalarm.app.database;

import com.bitcoinalarm.app.ExchangeAPI.MtGoxClient;
import com.bitcoinalarm.app.domens.Alarm;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 08.04.13
 * Time: 3:45
 * To change this template use File | Settings | File Templates.
 */
public class AlarmDAO extends BaseDaoImpl<Alarm, Integer> {
    public AlarmDAO(ConnectionSource connectionSource, Class<Alarm> alarmClass) throws SQLException {
        super(connectionSource, alarmClass);
    }

    public List<Alarm> getAllAlarms() throws SQLException {
        return this.queryForAll();
    }

    public Alarm getAlarmWithMinPeriodTime() throws SQLException {
        QueryBuilder<Alarm, Integer> queryBuilder = queryBuilder();

        queryBuilder.orderBy("periodTime", true);
        queryBuilder.limit(1);

        PreparedQuery<Alarm> preparedQuery = queryBuilder.prepare();
        Alarm alarm = queryForFirst(preparedQuery);
        return alarm;
    }



    public Alarm getAlarmWithMaxPeriodTime() throws SQLException {
        QueryBuilder<Alarm, Integer> queryBuilder = queryBuilder();
        queryBuilder.orderBy("periodTime", false);
        queryBuilder.limit(1);

        PreparedQuery<Alarm> preparedQuery = queryBuilder.prepare();
        Alarm alarm = queryForFirst(preparedQuery);
        return alarm;
    }

    public List<String> getCurrencyList() throws SQLException {
        List<String> currencyList = new ArrayList<String>();
        for (Alarm alarm : getAllAlarms()) {
            if (currencyList.indexOf(MtGoxClient.CurrencyArray[alarm.getCurrencyID()]) == -1)
                currencyList.add(MtGoxClient.CurrencyArray[alarm.getCurrencyID()]);
        }
        return currencyList;
    }


    public Alarm getAlarmByID(int id) throws SQLException {
        QueryBuilder<Alarm, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq("id", id);

        PreparedQuery<Alarm> preparedQuery = queryBuilder.prepare();
        Alarm alarm = queryForFirst(preparedQuery);
        return alarm;
    }

    public Alarm getActiveCurrencyList(int id) throws SQLException {
        QueryBuilder<Alarm, Integer> queryBuilder = queryBuilder();

        PreparedQuery<Alarm> preparedQuery = queryBuilder.prepare();
        Alarm alarm = queryForFirst(preparedQuery);
        return alarm;
    }
}
