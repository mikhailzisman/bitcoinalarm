package com.bitcoinalarm.app.database;

import android.app.Activity;
import com.bitcoinalarm.app.domens.Alarm;
import com.bitcoinalarm.app.domens.EntityState;
import com.bitcoinalarm.app.interfaces.IAlarmListModel;
import com.bitcoinalarm.app.interfaces.IDisplayAlarm;
import com.bitcoinalarm.app.interfaces.IListModelData;

import java.sql.Date;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 05.04.13
 * Time: 2:20
 * To change this template use File | Settings | File Templates.
 */
public class AlarmListModel
        implements IAlarmListModel,
        IListModelData<Alarm> {
    private ArrayList<Alarm> alarmArray = new ArrayList<Alarm>();
    private ArrayList<Alarm> alarmArrayDeleted = new ArrayList<Alarm>();
    private IDisplayAlarm editAlarm;
    private Activity activity;


    @Override
    public synchronized IDisplayAlarm getNextDisplayAlarm(Date curTime) {
        return new Alarm();
    }

    @Override
    public synchronized Alarm createAlarm() {
        Alarm alarm = new Alarm();
        alarm.setId(0);

        //Признак для репозитория, что объект надо создать в БД,
        //а его ID заменить сгенереным из базы
        alarm.setState(EntityState.ADDED);

        //repeatUpdaterReciever.setName("New repeatUpdaterReciever");//Название

        return alarm;
    }

    @Override
    public synchronized void addAlarm(IDisplayAlarm item) {
        Alarm alarm = (Alarm) item;
        Exception ex;

        if (item.getId() > 0) {
            ex = new Exception("Попытка добавить запись с ключем");
        }

        alarm.setState(EntityState.ADDED);
        alarmArray.add(alarm);
    }

    @Override
    public synchronized void updateAlarm(IDisplayAlarm item) {

        Alarm alarm = (Alarm) item;

        alarm.setState(EntityState.CHANGED);

        alarmArray.set(getPositionByID(item.getId()), alarm);

    }

    private int getPositionByID(int id) {

        for (Alarm alarm : alarmArray) {
            if (alarm.getId() == id)
                return alarmArray.indexOf(alarm);
        }

        return 0;
    }

    @Override
    public synchronized void deleteAlarm(IDisplayAlarm item) {
        Alarm alarm = (Alarm) item;

        alarmArray.remove(alarm);

        alarm.setState(EntityState.DELETED);
        alarmArrayDeleted.add(alarm);
    }

    public synchronized void takeAlarmForEdit(int alarmID) {
        if (getDisplayAlarmByID(alarmID) != null)
            editAlarm = ((Alarm) getDisplayAlarmByID(alarmID));//.clone()
        else {
            editAlarm = createAlarm();
        }

    }


    private Alarm getDisplayAlarmByID(int alarmID) {
        if ((alarmID < alarmArray.size()) && (alarmID >= 0))
            return alarmArray.get(alarmID);
        else return null;

    }

    @Override
    public synchronized Alarm getEditingAlarm() {
        return (Alarm) editAlarm;
    }

    @Override
    public synchronized void saveEditingAlarm(boolean stayEditing) {
        if (editAlarm == null) return;

        if (editAlarm.getId() > 0)
            updateAlarm(editAlarm);
        else
            addAlarm(editAlarm);

        if (!stayEditing) resetEditingAlarm();
    }

    @Override
    public void resetEditingAlarm() {
        editAlarm = null;
    }

    @Override
    public synchronized ArrayList<Alarm> getItemList() {
        return alarmArray;
    }

    @Override
    public synchronized ArrayList<Alarm> getDeletedItemList() {
        return alarmArrayDeleted;
    }

}
