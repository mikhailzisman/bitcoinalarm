package com.bitcoinalarm.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bitcoinalarm.app.Activities.AddAlarmActivity;
import com.bitcoinalarm.app.R;
import com.bitcoinalarm.app.database.AlarmRepository;
import com.bitcoinalarm.app.domens.Alarm;
import com.bitcoinalarm.app.domens.EntityState;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 14.04.13
 * Time: 16:53
 * To change this template use File | Settings | File Templates.
 */
public class ItemAlarmAdapter extends ArrayAdapter<Alarm>  {
    private Context context;
    private List<Alarm> alarms;

    public ItemAlarmAdapter(Context context, int resource, int textViewResourceId, List<Alarm> alarms) {
        super(context, resource, textViewResourceId, alarms);
        this.context = context;
        this.alarms = alarms;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.alarm_listitem, null);
        }

        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                "fonts/Roboto-Regular.ttf");


        TextView tvAlarmName = (TextView) convertView.findViewById(R.id.tvAlarmName);
        tvAlarmName.setTypeface(tf);
        tvAlarmName.setText(Html.fromHtml(alarms.get(position).alarmSettingstoHtmlString()));
        ImageView ivEditAlarm = (ImageView) convertView.findViewById(R.id.btEditAlarm);

        ivEditAlarm.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                AlarmRepository.getAlarmListModel().takeAlarmForEdit(position);
                Intent startAddAlarmIntent = new Intent(context, AddAlarmActivity.class);
                context.startActivity(startAddAlarmIntent);
                notifyDataSetChanged();
            }
        });
        ivEditAlarm.setImageResource(R.drawable.edit);

        ImageView ivActivateAlarm = (ImageView) convertView.findViewById(R.id.btActivateAlarm);

        switch (alarms.get(position).getTypeOfNotificationID()){
            case 0:
                if (alarms.get(position).isEnable())
                    ivActivateAlarm.setImageResource(R.drawable.alarm_enabled);
                else ivActivateAlarm.setImageResource(R.drawable.alarm_disabled);
                break;
            case 1:
                if (alarms.get(position).isEnable())
                    ivActivateAlarm.setImageResource(R.drawable.notification_active);
                else ivActivateAlarm.setImageResource(R.drawable.notification_disabled);
                break;
        }

        ivActivateAlarm.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                alarms.get(position).setEnable(!alarms.get(position).isEnable());
                alarms.get(position).setState(EntityState.CHANGED);
                AlarmRepository.getInstance().save();
                notifyDataSetChanged();
            }
        });

        return convertView;

    }
}
