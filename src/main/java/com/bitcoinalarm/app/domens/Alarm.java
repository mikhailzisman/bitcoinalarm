package com.bitcoinalarm.app.domens;

import com.bitcoinalarm.app.ExchangeAPI.MtGoxClient;
import com.bitcoinalarm.app.database.HelperFactory;
import com.bitcoinalarm.app.interfaces.IDisplayAlarm;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 05.04.13
 * Time: 2:20
 * To change this template use File | Settings | File Templates.
 */

@DatabaseTable(tableName = "alarms")
public class Alarm implements IDisplayAlarm, Serializable{

    @DatabaseField(generatedId = true)
    private int id;
    private EntityState state;
    private long lastSwitchesTime;
    @DatabaseField(dataType = DataType.BOOLEAN)
    private boolean enable;
    @DatabaseField(dataType = DataType.INTEGER)
    private int exchangeID;
    @DatabaseField(dataType = DataType.INTEGER)
    private int currencyID;
    @DatabaseField(dataType = DataType.INTEGER)
    private int reasonAlertID;
    @DatabaseField(dataType = DataType.INTEGER)
    private int resultTypeID;
    @DatabaseField(dataType = DataType.DOUBLE)
    private double edgePrice;
    @DatabaseField(dataType = DataType.INTEGER)
    private int periodTime;
    @DatabaseField(dataType = DataType.INTEGER)
    private int periodTypeID;
    @DatabaseField(dataType = DataType.BOOLEAN)
    private boolean soundEnabled;
    @DatabaseField(dataType = DataType.INTEGER)
    private int SoundID;
    @DatabaseField(dataType = DataType.INTEGER)
    private int volumeLevel;
    @DatabaseField(dataType = DataType.BOOLEAN)
    private boolean Vibrate;
    @DatabaseField(dataType = DataType.INTEGER)
    private int typeOfNotificationID;


    public Alarm(EntityState state, boolean enable, int exchangeID, int reasonAlertID, double edgePrice, int soundID, int volumeLevel, boolean vibrate) {
        this.state = state;
        this.enable = enable;
        this.exchangeID = exchangeID;
        this.reasonAlertID = reasonAlertID;
        this.edgePrice = edgePrice;
        this.SoundID = soundID;
        this.volumeLevel = volumeLevel;
        this.Vibrate = vibrate;
    }

    public Alarm() {
    }

    public Alarm(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EntityState getState() {
        return state;
    }

    public void setState(EntityState state) {
        this.state = state;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public int getExchangeID() {
        return exchangeID;
    }

    public void setExchangeID(int exchangeID) {
        this.exchangeID = exchangeID;
    }

    public int getReasonAlertID() {
        return reasonAlertID;
    }

    public void setReasonAlertID(int reasonAlertID) {
        this.reasonAlertID = reasonAlertID;
    }

    public double getEdgePrice() {
        return edgePrice;
    }

    public void setEdgePrice(double edgePrice) {
        this.edgePrice = edgePrice;
    }

    public int getSoundID() {
        return SoundID;
    }

    public void setSoundID(int soundID) {
        SoundID = soundID;
    }

    public int getVolumeLevel() {
        return volumeLevel;
    }

    public void setVolumeLevel(int volumeLevel) {
        this.volumeLevel = volumeLevel;
    }

    public boolean isVibrate() {
        return Vibrate;
    }

    public void setVibrate(boolean vibrate) {
        Vibrate = vibrate;
    }

    public int getPeriodTimeInUnit() {
        return periodTime;
    }

    public void setPeriodTimeInUnit(int periodTime) {
        this.periodTime = periodTime;
    }

    public int getCurrencyID() {
        return currencyID;
    }

    public void setCurrencyID(int currencyID) {
        this.currencyID = currencyID;
    }

    public long getLastSwitchesTime() {
        return lastSwitchesTime;
    }

    public void setLastSwitchesTime(long lastSwitchesTime) {
        this.lastSwitchesTime = lastSwitchesTime;
    }

    public int getResultTypeID() {
        return resultTypeID;
    }

    public void setResultTypeID(int resultTypeID) {
        this.resultTypeID = resultTypeID;
    }


    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append("When ");
        sb.append(MtGoxClient.ReasonArray[getReasonAlertID()] + " ");
        if (getReasonAlertID() < 2) {
            sb.append(getEdgePrice()+" ");
            sb.append(MtGoxClient.CurrencyArray[getCurrencyID()]);
        }

        if (getReasonAlertID() >= 2) {
            sb.append(getEdgePrice() +" ");
            sb.append(MtGoxClient.CurrencyArray[getCurrencyID()]);
            sb.append(" in " + getPeriodTimeInUnit()
                    + " " + MtGoxClient.PeriodTypesArray[getPeriodTypeID()]);
        }

        return sb.toString();
    }

    public boolean isSoundEnabled() {
        return soundEnabled;
    }

    public void setSoundEnabled(boolean soundEnabled) {
        this.soundEnabled = soundEnabled;
    }

    public int getPeriodTypeID() {
        return periodTypeID;
    }

    public void setPeriodTypeID(int periodTypeID) {
        this.periodTypeID = periodTypeID;
    }

    public long getPeriodTimeInSecond() {

        return Utils.getPeriodInSeconds(getPeriodTypeID(), getPeriodTimeInUnit());

    }

    public int getTypeOfNotificationID() {
        return typeOfNotificationID;
    }

    public void setTypeOfNotificationID(int typeOfNotificationID) {
        this.typeOfNotificationID = typeOfNotificationID;
    }


    public String alarmSettingstoHtmlString() {

        StringBuilder sb = new StringBuilder();
        sb.append("When ");
        sb.append("<b>");
        sb.append(MtGoxClient.ReasonArray[getReasonAlertID()] + " ");
        //sb.append("<br/>");
        if (getReasonAlertID() < 2) {
            sb.append(getEdgePrice()+" ");
            sb.append(MtGoxClient.CurrencyArray[getCurrencyID()]);
        }

        if (getReasonAlertID() >= 2) {

            sb.append(getEdgePrice() +" ");

            sb.append(MtGoxClient.CurrencyArray[getCurrencyID()]);
            sb.append(" in " + getPeriodTimeInUnit()
                    + " " + MtGoxClient.PeriodTypesArray[getPeriodTypeID()]);
        }

        sb.append(" on " + MtGoxClient.ExchangeArray[getExchangeID()]);
        sb.append("</b>");

        return sb.toString();
    }


    public String alarmResultToHtml() {

        MtGoxResult lastResult = null;
        try {
            lastResult =  HelperFactory.getHelper().getMtGoxResultDAO().getLastResultByAlarm(this);
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        String textForOutput = "<font color=#979797> BTC "
                + MtGoxClient.ResultArray[getReasonAlertID()] + "</font>";
        switch (getReasonAlertID()) {
            case 0:
                textForOutput +=
                        "<b> <font color=#00FF00>" + getEdgePrice() + "</font> "
                        + MtGoxClient.CurrencyArray[getCurrencyID()] + " </b>"
                        + "<font color=#979797> on </font> "
                        +"<b>"+ MtGoxClient.ExchangeArray[getExchangeID()]+" </b>";

                break;
            case 1:
                textForOutput +=
                        "<b> <font color=#FF0000>" + getEdgePrice() + "</font> "
                        + MtGoxClient.CurrencyArray[getCurrencyID()] + " </b>"
                        + "<font color=#979797> on </font> "
                        +"<b>"+ MtGoxClient.ExchangeArray[getExchangeID()]+" </b>";

                break;
            case 2:
                textForOutput +=
                        "<b> <font color=#00FF00>" + getEdgePrice()
                        + MtGoxClient.CurrencyArray[getCurrencyID()] + " </font></b> "
                        + " <font color=#979797> over the past </font>"
                        + "<b>"+ getPeriodTimeInUnit() + " "+ MtGoxClient.PeriodTypesArray[getPeriodTypeID()]+"</b>"
                        + "<font color=#979797> on </font> "
                        + "<b>"+ MtGoxClient.ExchangeArray[getExchangeID()]+"</b>"
                        + " <font color=#979797> and is now </font>"
                        + "<b> <font color=#00FF00>" + lastResult.getValue()+" "
                        + MtGoxClient.CurrencyArray[getCurrencyID()] + " </font></b> ";
                break;
            case 3:


                textForOutput +=
                        "<b> <font color=#00FF00>" + getEdgePrice()
                        + MtGoxClient.CurrencyArray[getCurrencyID()] + " </font></b> "
                        + " <font color=#979797> over the past </font>"
                        + "<b>"+ getPeriodTimeInUnit() + " "+ MtGoxClient.PeriodTypesArray[getPeriodTypeID()]+"</b>"
                        + "<font color=#979797> on </font> "
                        + "<b>"+ MtGoxClient.ExchangeArray[getExchangeID()]+"</b>"
                        + " <font color=#979797> and is now </font>"
                        + "<b> <font color=#FF0000>" + lastResult.getValue()+" "
                        + MtGoxClient.CurrencyArray[getCurrencyID()] + " </font></b> ";
                break;
        }

        textForOutput = textForOutput.replace("USD","$");
        textForOutput = textForOutput.replace("EUR","€");
        return textForOutput;
    }


}
