package com.bitcoinalarm.app.domens;

import android.content.Context;
import android.content.SharedPreferences;
import com.bitcoinalarm.app.Utils.Settings;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 22.04.13
 * Time: 22:01
 * To change this template use File | Settings | File Templates.
 */
public class Utils {

    public static long getPeriodInSeconds(int periodTypeID, int periodInUnits) {
        switch (periodTypeID) {
            case 0:
                return periodInUnits;
            case 1:
                return periodInUnits * 60;
            case 2:
                return periodInUnits * 3600;


        }

        return Integer.MAX_VALUE;
    }

    public static long getRefreshRateFromSharedPreferenceInSeconds(Context context) {
        SharedPreferences mSharedPreference = context.getSharedPreferences(Settings.MY_PREF, Context.MODE_PRIVATE);
        int refreshRateInUnits = mSharedPreference.getInt(Settings.refreshRateInUnits, Integer.MAX_VALUE);
        int refreshRateTypeID = mSharedPreference.getInt(Settings.refreshRateTypeID, 0);
        return getPeriodInSeconds(refreshRateTypeID, refreshRateInUnits);
    }

    public static void saveSharedPreferenceFromAlarm(Context context, Alarm alarm) {
        SharedPreferences mSharedPreference = context.getSharedPreferences(Settings.MY_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSharedPreference.edit();
        editor.putInt(Settings.refreshRateTypeID, alarm.getPeriodTypeID());
        editor.putInt(Settings.refreshRateInUnits, alarm.getPeriodTimeInUnit());
        editor.commit();
    }

    public static long getLaunchTime(Context context) {
        SharedPreferences mSharedPreference = context.getSharedPreferences(Settings.MY_PREF, Context.MODE_PRIVATE);
        return mSharedPreference.getLong(Settings.launchTime, 0);
    }

    public static void saveLaunchTime(Context context) {
        SharedPreferences mSharedPreference = context.getSharedPreferences(Settings.MY_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSharedPreference.edit();
        editor.putLong(Settings.launchTime, System.currentTimeMillis());
        editor.commit();
    }

    private void saveLaunchTimeToFile(File file, long time) {
        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(String.valueOf(time).getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String loadLaunchTime(File file) {
        String myData = "";
        try {
            FileInputStream fis = new FileInputStream(file);
            DataInputStream in = new DataInputStream(fis);
            BufferedReader br =
                    new BufferedReader(new InputStreamReader(in));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                myData = myData + strLine;
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return myData;
    }
}
