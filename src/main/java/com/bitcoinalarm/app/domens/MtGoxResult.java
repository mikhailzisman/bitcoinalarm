package com.bitcoinalarm.app.domens;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 08.04.13
 * Time: 0:48
 * To change this template use File | Settings | File Templates.
 */
@DatabaseTable(tableName = "mtgoxresults")
public class MtGoxResult {
    @DatabaseField(generatedId = true)
    private int id;


    @DatabaseField(dataType = DataType.INTEGER)
    private int alarmID;
    @DatabaseField(dataType = DataType.STRING)
    private String resultType;
    @DatabaseField(dataType = DataType.DOUBLE)
    private double value;
    @DatabaseField(dataType = DataType.STRING)
    private String display;
    @DatabaseField(dataType = DataType.INTEGER)
    private int value_int;
    @DatabaseField(dataType = DataType.STRING)
    private String display_shot;
    @DatabaseField(dataType = DataType.STRING)
    private String currency;
    @DatabaseField(dataType = DataType.LONG)
    private long timeNow;

    public MtGoxResult() {
    }

    public MtGoxResult(String resultType, Double value, String display, Integer value_int, String display_shot, String currency, Long timeNow) {
        this.resultType = resultType;

        this.value = value;
        this.display = display;
        this.value_int = value_int;
        this.display_shot = display_shot;
        this.currency = currency;
        this.timeNow = timeNow;
    }

    public Long getTimeNow() {
        return timeNow;
    }

    public void setTimeNow(Long timeNow) {
        this.timeNow = timeNow;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public Integer getValue_int() {
        return value_int;
    }

    public void setValue_int(Integer value_int) {
        this.value_int = value_int;
    }

    public String getDisplay_shot() {
        return display_shot;
    }

    public void setDisplay_shot(String display_shot) {
        this.display_shot = display_shot;
    }

    public String getCurrencyID() {
        return currency;
    }

    public void setCurrencyID(String currencyID) {
        this.currency = currency;
    }

    public int getAlarmID() {
        return alarmID;
    }

    public void setAlarmID(int alarmID) {
        this.alarmID = alarmID;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }
}
