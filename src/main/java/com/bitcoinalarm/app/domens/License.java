package com.bitcoinalarm.app.domens;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 06.05.13
 * Time: 12:36
 * To change this template use File | Settings | File Templates.
 */
@DatabaseTable(tableName = "license")
public class License {

    public final static int REAL_LICENSE = 0;

    public final static int TRIAL_LICENSE = 1;


    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(dataType = DataType.STRING)
    private String license;

    //0 - real license
    //1 - trial license
    @DatabaseField(dataType = DataType.INTEGER)
    private int licenseType;

    @DatabaseField(dataType = DataType.LONG)
    private long timeOfRegistration;

    public License() {
    }

    public License(String license, int licenseType, long timeOfRegistration){
        this.license = license;
        this.licenseType = licenseType;
        this.timeOfRegistration = timeOfRegistration;
    }



    public String getLicense() {
        return license;
    }

    public int getLicenseType() {
        return licenseType;
    }

    public long getTimeOfRegistration() {
        return timeOfRegistration;
    }
}
