package com.bitcoinalarm.app.domens;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 05.04.13
 * Time: 2:28
 * To change this template use File | Settings | File Templates.
 */
public enum EntityState {
    ADDED,
    NOT_CHANGED,
    CHANGED,
    DELETED
}