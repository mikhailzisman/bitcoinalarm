package com.bitcoinalarm.app.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.bitcoinalarm.app.ExchangeAPI.MtGoxClient;
import com.bitcoinalarm.app.R;
import com.bitcoinalarm.app.Utils.LicenseChecker;
import com.bitcoinalarm.app.Utils.Settings;
import com.bitcoinalarm.app.domens.Utils;
import com.bitcoinalarm.app.interfaces.ICheckerLicenser;
import com.bitcoinalarm.app.interfaces.IListenerRepositoryChanged;
import com.bitcoinalarm.app.service.UpdaterControl;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 05.04.13
 * Time: 14:21
 * To change this template use File | Settings | File Templates.
 */
public class SettingsFragment extends Fragment implements ICheckerLicenser {

    private ArrayAdapter<String> spRefreshRateTypesAdapter;
    private Spinner spRefreshRateType;
    private Button btSaveSettings;
    private SharedPreferences mySharedPreferences;
    private EditText edRefreshRate;
    private CheckBox cbCheckWhenWiFi;
    private IListenerRepositoryChanged listenerRepositoryChanged = UpdaterControl.getInstanse();
    private AlertDialog alertDialog;
    private ProgressDialog progressDialog;
    private LicenseChecker licenseChecker;
    private Button btRegister;
    private TextView tvNotRegister;



    private enum State {Registred, NotRegisterd, TrialRegisterd} ;


    @Override
    public void onCheckResult(boolean result) {
        progressDialog.dismiss();
        alertDialog.dismiss();
        if (result) setUIState(State.Registred);
        else setUIState(State.NotRegisterd);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.settings_fragment, container, false);

        spRefreshRateType = (Spinner) view.findViewById(R.id.spRefreshRateSecMinHour);
        spRefreshRateTypesAdapter = new ArrayAdapter<String>(getActivity(), R.layout.currency_text_layout, R.id.tvRowItem, MtGoxClient.PeriodTypesArray);
        spRefreshRateType.setAdapter(spRefreshRateTypesAdapter);

        edRefreshRate = (EditText) view.findViewById(R.id.edRefreshRate);

        cbCheckWhenWiFi = (CheckBox) view.findViewById(R.id.checkWhenWiFi);

        btSaveSettings = (Button) view.findViewById(R.id.btSaveSettings);
        btSaveSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.getPeriodInSeconds(spRefreshRateType.getSelectedItemPosition(),
                        Integer.parseInt(edRefreshRate.getText().toString()))
                        < Settings.minRefreshTimeInSeconds) {
                    Toast.makeText(getActivity(), "Please, enter period time more than " + Settings.minRefreshTimeInSeconds + " seconds", 3000).show();
                    return;
                }

                SharedPreferences.Editor editor = mySharedPreferences.edit();
                editor.putInt("RefreshRate", Integer.parseInt(edRefreshRate.getText().toString()));
                editor.putInt("RefreshRateTypeID", spRefreshRateType.getSelectedItemPosition());
                editor.putBoolean("CheckWifi", cbCheckWhenWiFi.isChecked());
                editor.commit();
                Toast.makeText(getActivity(), "Settings saved", 3000).show();
                listenerRepositoryChanged.onAlarmUpdated(getActivity());
            }
        });

        //Register button

        btRegister = (Button) view.findViewById(R.id.btRegister);
        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRegistrationDialog();
            }
        });
        tvNotRegister = (TextView) view.findViewById(R.id.tvNotRegister);
        tvNotRegister.setMovementMethod(LinkMovementMethod.getInstance());
        tvNotRegister.setText(Html.fromHtml(getActivity().getString(R.string.trial_version_message)));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadPref();

        if (licenseChecker == null)
            licenseChecker = new LicenseChecker(this);

        if (licenseChecker.isLicensedByRealLicense())
            setUIState(State.Registred);
        else if (licenseChecker.isLicenedByTrial()) {
            setUIState(State.TrialRegisterd);
        } else
            setUIState(State.NotRegisterd);
    }

    private void setUIState(State registredState) {
        switch (registredState) {
            case Registred:
                btRegister.setVisibility(View.GONE);
                tvNotRegister.setVisibility(View.GONE);
                break;

            case TrialRegisterd:
                btRegister.setVisibility(View.VISIBLE);
                tvNotRegister.setVisibility(View.VISIBLE);
                tvNotRegister.setText(Html.fromHtml(getActivity().getString(R.string.trial_version_message)));
                break;
            case NotRegisterd:
                btRegister.setVisibility(View.VISIBLE);
                tvNotRegister.setVisibility(View.VISIBLE);
                tvNotRegister.setText(R.string.registration_key_not_valid);
                break;
        }
    }

    private void loadPref() {
        mySharedPreferences = getActivity().getSharedPreferences(Settings.MY_PREF, Context.MODE_PRIVATE);
        int val = mySharedPreferences.getInt(Settings.refreshRateInUnits, 60);

        edRefreshRate.setText(String.valueOf(val));

        spRefreshRateType.setSelection(mySharedPreferences.getInt(Settings.refreshRateTypeID, 0));
        cbCheckWhenWiFi.setChecked(mySharedPreferences.getBoolean(Settings.checkWifi, true));
    }

    private void showRegistrationDialog() {


        alertDialog = new AlertDialog.Builder(getActivity()).create();

        try {
            alertDialog.setTitle(getString(R.string.registration));
            alertDialog.setMessage(getString(R.string.content_registration));

            alertDialog.setView(getActivity().getLayoutInflater().inflate(R.layout.registration_input, null));

            alertDialog.setCancelable(true);
            alertDialog.setCanceledOnTouchOutside(true);
            //listeners will be overrided by RegisterButtonListener
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.button_register),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

            alertDialog.show();

            Button btRegistration = alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL);

            btRegistration.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (com.bitcoinalarm.app.Utils.Utils.isOnline(getActivity())) {
                        EditText edRegistrationKey = (EditText) alertDialog.findViewById(R.id.edRegKey);
                        if (!edRegistrationKey.getText().toString().equals("")) {
                            progressDialog = ProgressDialog.show(getActivity(), "", "Checking...", false, true);
                            licenseChecker.activate(edRegistrationKey.getText().toString());
                        } else Toast.makeText(getActivity(), getString(R.string.checkYourInput), 3000).show();
                    } else Toast.makeText(getActivity(), getString(R.string.internetIsNotAval), 3000).show();
                }
            });
        } catch (Exception e) {
            //Handle BadTokenException.
        }
    }


}
