package com.bitcoinalarm.app.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import com.bitcoinalarm.app.Activities.AddAlarmActivity;
import com.bitcoinalarm.app.Adapter.ItemAlarmAdapter;
import com.bitcoinalarm.app.R;
import com.bitcoinalarm.app.database.AlarmRepository;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 05.04.13
 * Time: 14:14
 * To change this template use File
 * | Settings | File Templates.
 */
public class AlarmsFragment extends Fragment implements View.OnClickListener
{


    Button btAddAlarm;
    private ListView lvAlarmsListView;
    private ItemAlarmAdapter itemAlarmAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.alarms_fragment, container, false);

        btAddAlarm = (Button) view.findViewById(R.id.btAddAlarm);
        btAddAlarm.setOnClickListener(this);

        lvAlarmsListView = (ListView) view.findViewById(R.id.AlarmsListView);
        itemAlarmAdapter = new ItemAlarmAdapter(
                getActivity(),
                R.layout.alarm_listitem,
                0,
                AlarmRepository.getAlarmListModel().getItemList());
        lvAlarmsListView.setAdapter(itemAlarmAdapter);

        lvAlarmsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                AlarmRepository.getAlarmListModel().takeAlarmForEdit(position);
                Intent startAddAlarmIntent = new Intent(getActivity(), AddAlarmActivity.class);
                getActivity().startActivity(startAddAlarmIntent);
            }
        });


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

        lvAlarmsListView.invalidateViews();
        itemAlarmAdapter.notifyDataSetChanged();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btAddAlarm:

                Intent startAddAlarmIntent = new Intent(getActivity(), AddAlarmActivity.class);

                startActivity(startAddAlarmIntent);

                break;
        }
    }

}
