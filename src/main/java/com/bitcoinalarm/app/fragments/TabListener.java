package com.bitcoinalarm.app.fragments;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentTransaction;
import com.bitcoinalarm.app.Activities.AlarmListActivity;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 05.04.13
 * Time: 14:29
 * To change this template use File | Settings | File Templates.
 */
public class TabListener<T> implements ActionBar.TabListener {


    private AlarmListActivity mActivity;
    private String string;
    private Class<T> fragmentClass;
    private Fragment mFragment;
    private String mTag = "TabListener";

    public TabListener(AlarmListActivity mainActivity, String string, Class<T> fragmentClass) {
        this.mActivity = mainActivity;
        this.string = string;
        this.fragmentClass = fragmentClass;


    }


    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // Check if the fragment is already initialized
        if (mFragment == null) {
            // If not, instantiate and add it to the activity
            mFragment = Fragment.instantiate(mActivity, fragmentClass.getName());
            fragmentTransaction.add(android.R.id.content, mFragment, mTag);
        } else {
            // If it exists, simply attach it in order to show it
            fragmentTransaction.attach(mFragment);
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        if (mFragment != null) {
            // Detach the fragment, because another one is being attached
            fragmentTransaction.detach(mFragment);
        }
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
