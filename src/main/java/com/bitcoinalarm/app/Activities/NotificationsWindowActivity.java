package com.bitcoinalarm.app.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import com.bitcoinalarm.app.R;
import com.bitcoinalarm.app.Utils.SoundService;
import com.bitcoinalarm.app.Utils.VibratorService;
import com.bitcoinalarm.app.domens.Alarm;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 26.04.13
 * Time: 18:16
 * To change this template use File | Settings | File Templates.
 */
public class NotificationsWindowActivity extends Activity {
    private String message;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.notification_alarm);

        Alarm alarm = (Alarm) getIntent().getSerializableExtra("alarm");

        message = alarm.alarmResultToHtml();

        message = message.replace("USD", "$");
        message = message.replace("EUR", "€");

        //start vibrate if need
        if (alarm.isVibrate())
            VibratorService.getInstance().startVibrate(
                    this,
                    (alarm.getTypeOfNotificationID() == 0) ? true : false);

        //start sound if need
        if (alarm.isSoundEnabled())
            SoundService.getInstance().playSound(
                    this,
                    alarm.getSoundID(),
                    alarm.getVolumeLevel(),
                    (alarm.getTypeOfNotificationID() == 0) ? true : false);

        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setIcon(R.drawable.drawable_hdpi_ic_launcher)
                .setTitle(R.string.NotificationTitle)
                .setMessage(Html.fromHtml(message))
                .setPositiveButton(R.string.dismiss, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        VibratorService.getInstance().stopVibrate();
                        SoundService.getInstance().stopSound();
                        //dialogInterface.dismiss();
                        gotoPreviusActivity();

                    }
                })
                .setNegativeButton(R.string.tweet, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        VibratorService.getInstance().stopVibrate();
                        SoundService.getInstance().stopSound();
                        try{
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            if (message != null)
                                intent.setData(Uri.parse("twitter://post?message=" + Uri.encode(Html.fromHtml(message).toString()+". via @coincliff")));
                            startActivity(intent);
                        }catch (ActivityNotFoundException ex){
                              //app is not installed
                        }
                    }
                }).show();
    }

    private void goToHomeScreen() {
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory(Intent.CATEGORY_HOME);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }

    private void gotoPreviusActivity() {
        Intent i= new Intent(this,AlarmListActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(i);
        finish();
    }

}
