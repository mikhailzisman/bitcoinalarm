package com.bitcoinalarm.app.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.*;
import com.bitcoinalarm.app.ExchangeAPI.MtGoxClient;
import com.bitcoinalarm.app.R;
import com.bitcoinalarm.app.Utils.Settings;
import com.bitcoinalarm.app.Utils.SoundService;
import com.bitcoinalarm.app.Utils.Utils;
import com.bitcoinalarm.app.database.AlarmRepository;
import com.bitcoinalarm.app.domens.Alarm;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 14.04.13
 * Time: 12:48
 * To change this template use File | Settings | File Templates.
 */
public class AddAlarmActivity extends Activity implements View.OnClickListener {


    private Activity context;
    private Spinner spExchanges;
    private Spinner spCurrency;
    private Spinner spEvent;
    private EditText edPrice;
    private EditText edPeriod;
    private ArrayAdapter<String> spCurrencyAdapter;
    private ArrayAdapter<String> spEventsAdapter;
    private Spinner spResultType;
    private ArrayAdapter<String> spExchangeTypeAdapter;
    private ArrayAdapter<String> spResultTypeAdapter;
    private CheckBox cbSoundCheckBox;
    private Spinner spSound;
    private ArrayAdapter<String> spSoundAdapter;
    private CheckBox cbVibrate;
    private SeekBar sbVolume;
    private ArrayAdapter<String> spPeriodTypesAdapter;
    private Spinner spPeriodType;
    private Spinner spTypeOfNotification;
    private ArrayAdapter<String> spTypeOfNotificationAdapter;
    private Button btTestSound;


    private enum SoundsState {ENABLE, DISABLE}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.alarm_add);
        Button saveButton = (Button) findViewById(R.id.saveButton);
        saveButton.setOnClickListener(this);


        Button deleteButton = (Button) findViewById(R.id.deleteButton);
        deleteButton.setOnClickListener(this);

        spExchanges = (Spinner) findViewById(R.id.spExchanges);
        spResultType = (Spinner) findViewById(R.id.spResultType);
        spCurrency = (Spinner) findViewById(R.id.spCurrency);
        spEvent = (Spinner) findViewById(R.id.spEvent);
        edPrice = (EditText) findViewById(R.id.editPrice);
        edPeriod = (EditText) findViewById(R.id.edRefreshRate);
        cbSoundCheckBox = (CheckBox) findViewById(R.id.cbSoundCheckBox);
        spSound = (Spinner) findViewById(R.id.spSound);
        sbVolume = (SeekBar) findViewById(R.id.sbSoundVolume);
        spPeriodType = (Spinner) findViewById(R.id.spRefreshRateSecMinHour);
        spTypeOfNotification = (Spinner) findViewById(R.id.spTypeOfNotification);
        btTestSound = (Button)findViewById(R.id.btTestSound);

        cbVibrate = (CheckBox) findViewById(R.id.cbVibrate);

        spExchangeTypeAdapter = new ArrayAdapter<String>(this, R.layout.currency_text_layout, R.id.tvRowItem, MtGoxClient.ExchangeArray);
        spExchanges.setAdapter(spExchangeTypeAdapter);


        spResultTypeAdapter = new ArrayAdapter<String>(this, R.layout.currency_text_layout, R.id.tvRowItem, MtGoxClient.TypeResultArray);
        spResultType.setAdapter(spResultTypeAdapter);

        spCurrencyAdapter = new ArrayAdapter<String>(this, R.layout.currency_text_layout, R.id.tvRowItem, MtGoxClient.CurrencyList);
        spCurrency.setAdapter(spCurrencyAdapter);

        spTypeOfNotificationAdapter = new ArrayAdapter<String>(this, R.layout.currency_text_layout, R.id.tvRowItem, MtGoxClient.NotificationTypeArray);
        spTypeOfNotification.setAdapter(spTypeOfNotificationAdapter);

        spEventsAdapter = new ArrayAdapter<String>(this, R.layout.currency_text_layout, R.id.tvRowItem, MtGoxClient.ReasonList);
        spEvent.setAdapter(spEventsAdapter);
        spEvent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                //if we choosen raise above or down we should disable period
                    spPeriodType.setEnabled(position>=2);
                    edPeriod.setEnabled(position>=2);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });

        spSoundAdapter = new ArrayAdapter<String>(this, R.layout.currency_text_layout, R.id.tvRowItem, SoundService.getInstance().getSoundList());
        spSound.setAdapter(spSoundAdapter);

        spPeriodTypesAdapter = new ArrayAdapter<String>(this, R.layout.currency_text_layout, R.id.tvRowItem, MtGoxClient.PeriodTypesArray);
        spPeriodType.setAdapter(spPeriodTypesAdapter);


        cbSoundCheckBox.setOnClickListener(this);
        btTestSound.setOnClickListener(this);

        //this for input will bee hidden
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        context = this;

    }

    private synchronized boolean isCorrectFillEditingAlarmFromUI() {
        Alarm editAlarm = AlarmRepository.getAlarmListModel().getEditingAlarm();
        editAlarm.setEnable(true);

        if (!checkInputTypeCorresponds()) return false;

        editAlarm.setResultTypeID(spResultType.getSelectedItemPosition());
        editAlarm.setExchangeID(spExchanges.getSelectedItemPosition());
        editAlarm.setCurrencyID(spCurrency.getSelectedItemPosition());
        editAlarm.setReasonAlertID(spEvent.getSelectedItemPosition());
        editAlarm.setEdgePrice(Double.valueOf(edPrice.getText().toString()));
        editAlarm.setPeriodTimeInUnit(Integer.valueOf(edPeriod.getText().toString()));
        editAlarm.setPeriodTypeID(spPeriodType.getSelectedItemPosition());
        editAlarm.setVibrate(cbVibrate.isChecked());
        editAlarm.setSoundEnabled(cbSoundCheckBox.isChecked());
        editAlarm.setSoundID(spSound.getSelectedItemPosition());
        editAlarm.setVolumeLevel(sbVolume.getProgress());
        editAlarm.setTypeOfNotificationID(spTypeOfNotification.getSelectedItemPosition());

        return true;
    }

    private boolean checkInputTypeCorresponds() {
        if (!Utils.isDouble(edPrice.getText().toString())||!Utils.isInteger(edPeriod.getText().toString())){
            Toast.makeText(this,"Please, check your inputs",3000).show();
            return false;
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        restoreAlarmInUI(AlarmRepository.getAlarmListModel().getEditingAlarm());

    }

    private void restoreAlarmInUI(Alarm editingAlarm) {

        if (editingAlarm != null) {

            spResultType.setSelection(editingAlarm.getResultTypeID());
            spExchanges.setSelection(editingAlarm.getExchangeID());
            spCurrency.setSelection(editingAlarm.getCurrencyID());
            spEvent.setSelection(editingAlarm.getReasonAlertID());
            edPrice.setText(String.valueOf(editingAlarm.getEdgePrice()));
            edPeriod.setText(String.valueOf(editingAlarm.getPeriodTimeInUnit()));
            spPeriodType.setSelection(editingAlarm.getPeriodTypeID());
            cbVibrate.setChecked(editingAlarm.isVibrate());
            spTypeOfNotification.setSelection(editingAlarm.getTypeOfNotificationID());

            if (editingAlarm.isSoundEnabled())
                this.setSoundState(editingAlarm, SoundsState.ENABLE);
            else this.setSoundState(editingAlarm, SoundsState.DISABLE);
        }

    }

    private boolean isEditingAlarmInputsCorrect(){

        if  (AlarmRepository.getAlarmListModel().getEditingAlarm().getPeriodTimeInSecond()< Settings.minRefreshTimeInSeconds){
            Toast.makeText(this,"Please, enter period time more than "+ Settings.minRefreshTimeInSeconds+" seconds",3000).show();
            return false;
        }

        if  (AlarmRepository.getAlarmListModel().getEditingAlarm().getEdgePrice()< Settings.minPrice){
            Toast.makeText(this,"Please, price more than "+ Settings.minPrice+" seconds",3000).show();
            return false;
        }

        return true;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.saveButton:

                if (AlarmRepository.getAlarmListModel().getEditingAlarm() == null)
                    AlarmRepository.getAlarmListModel().takeAlarmForEdit(-1); // Create new Alarm

                if (!isCorrectFillEditingAlarmFromUI()) return;

                if (!isEditingAlarmInputsCorrect())  return;

                AlarmRepository.getAlarmListModel().saveEditingAlarm(false);
                AlarmRepository.getInstance().save();
                Toast.makeText(context, "Saved alarm", 3000);
                gotoPreviusActivity();
                break;
            case R.id.deleteButton:
                AlarmRepository.getAlarmListModel().deleteAlarm(
                        AlarmRepository.getAlarmListModel().getEditingAlarm());
                AlarmRepository.getInstance().save();
                Toast.makeText(context, "Deleted alarm", 3000);
                this.onBackPressed();
                break;
            case R.id.cbSoundCheckBox:
                spSound.setEnabled(cbSoundCheckBox.isChecked());
                sbVolume.setEnabled(cbSoundCheckBox.isChecked());
                btTestSound.setEnabled(cbSoundCheckBox.isChecked());
                break;
            case R.id.btTestSound:
                SoundService.getInstance().stopSound();
                SoundService.getInstance().playSound(context,
                        spSound.getSelectedItemPosition(),
                        sbVolume.getProgress(),false);
                break;
        }
    }

    private void gotoPreviusActivity() {
        Intent i= new Intent(this,AlarmListActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(i);
        finish();
    }

    private void setSoundState(Alarm editingAlarm, SoundsState soundState) {
        switch (soundState) {
            case ENABLE:
                sbVolume.setEnabled(true);
                spSound.setEnabled(true);
                cbSoundCheckBox.setChecked(true);

                sbVolume.setProgress(editingAlarm.getVolumeLevel());
                cbSoundCheckBox.setChecked(editingAlarm.isSoundEnabled());
                spSound.setSelection(editingAlarm.getSoundID());
                break;
            case DISABLE:
                cbSoundCheckBox.setChecked(false);

                sbVolume.setEnabled(false);
                spSound.setEnabled(false);
                break;
        }
    }


    @Override
    public void onBackPressed() {
        AlarmRepository.getAlarmListModel().resetEditingAlarm();
        super.onBackPressed();
    }
}
