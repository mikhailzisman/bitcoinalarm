package com.bitcoinalarm.app.Activities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.bitcoinalarm.app.R;
import com.bitcoinalarm.app.Utils.SoundService;
import com.bitcoinalarm.app.Utils.VibratorService;
import com.bitcoinalarm.app.domens.Alarm;


/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 26.04.13
 * Time: 12:50
 * To change this template use File | Settings | File Templates.
 */
public class AlarmWindowActivity extends Activity implements View.OnClickListener {
    private ImageView btDismiss;
    private ImageView btTweet;
    private TextView tvAlarmText;
    private ImageView btStop;
    private String message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.alarm_window);

        Alarm alarm = (Alarm) getIntent().getSerializableExtra("alarm");
        message = alarm.alarmResultToHtml();


        btDismiss = (ImageView) findViewById(R.id.btDismissAlarm);
        btDismiss.setOnClickListener(this);

        btTweet = (ImageView) findViewById(R.id.btTweet);
        btTweet.setOnClickListener(this);

        btStop = (ImageView) findViewById(R.id.btStopAlarm);
        btStop.setOnClickListener(this);

        tvAlarmText = (TextView) findViewById(R.id.alarmText);
        tvAlarmText.setText(Html.fromHtml(message));

        setStateOfViews(State.Stop);
        //start vibrate if need
        if (alarm.isVibrate())
            VibratorService.getInstance().startVibrate(
                    this,
                    (alarm.getTypeOfNotificationID() == 0) ? true : false);

        //start sound if need
        if (alarm.isSoundEnabled())
            SoundService.getInstance().playSound(
                    this,
                    alarm.getSoundID(),
                    alarm.getVolumeLevel(),
                    (alarm.getTypeOfNotificationID() == 0) ? true : false);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btStopAlarm:
                setStateOfViews(State.Dismiss);
                VibratorService.getInstance().stopVibrate();
                SoundService.getInstance().stopSound();
                break;
            case R.id.btTweet:
                try{
                Intent intent = new Intent(Intent.ACTION_VIEW);
                if (message != null)
                    intent.setData(Uri.parse("twitter://post?message=" + Uri.encode(Html.fromHtml(message).toString()+". via @coincliff")));
                startActivity(intent);
                }catch (ActivityNotFoundException ex){
                    //app is not installed
                }
                break;
            case R.id.btDismissAlarm:
                gotoAlarmListActivity();
                break;
        }
    }

    private void setStateOfViews(State state) {
        switch (state) {
            case Dismiss:
                btStop.setVisibility(View.GONE);
                btTweet.setVisibility(View.VISIBLE);
                btDismiss.setVisibility(View.VISIBLE);

                break;
            case Stop:
                btStop.setVisibility(View.VISIBLE);
                btTweet.setVisibility(View.GONE);
                btDismiss.setVisibility(View.GONE);

                break;
        }
    }

    private void gotoAlarmListActivity() {
        Intent i = new Intent(this, AlarmListActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(i);
        finish();
    }

    enum State {Stop, Dismiss}

    ;

}
