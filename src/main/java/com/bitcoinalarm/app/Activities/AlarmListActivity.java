
package com.bitcoinalarm.app.Activities;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.bitcoinalarm.app.R;
import com.bitcoinalarm.app.Utils.LicenseChecker;
import com.bitcoinalarm.app.Utils.Utils;
import com.bitcoinalarm.app.database.AlarmRepository;
import com.bitcoinalarm.app.database.DatabaseHelper;
import com.bitcoinalarm.app.fragments.AlarmsFragment;
import com.bitcoinalarm.app.fragments.SettingsFragment;
import com.bitcoinalarm.app.fragments.TabListener;
import com.bitcoinalarm.app.interfaces.ICheckerLicenser;
import com.bitcoinalarm.app.service.ExchangesServiceControl;
import com.bugsense.trace.BugSenseHandler;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.licel.stringer.annotations.secured;

@secured
public class AlarmListActivity
        extends OrmLiteBaseActivity<DatabaseHelper> implements ICheckerLicenser {

    private LicenseChecker licenseChecker;
    private AlertDialog alertDialog;
    private ProgressDialog progressDialog;
    private AlarmListActivity context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        BugSenseHandler.initAndStartSession(this, "8eb84d70");

        this.context = this;

        initUI();

        AlarmRepository.getInstance().load();

    }

    @Override
    protected void onStart() {

        super.onStart();

        licenseChecker = new LicenseChecker(this);

        if (!licenseChecker.isRegistredFromDatabase()) {

            showRegistrationDialogIfNeed();

        }
    }

    private void showRegistrationDialogIfNeed() {


        alertDialog = new AlertDialog.Builder(this).create();

        try {
            alertDialog.setTitle(getString(R.string.registration));
            alertDialog.setMessage(getString(R.string.content_registration));

            alertDialog.setView(getLayoutInflater().inflate(R.layout.registration_input, null));

            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            //listeners will be overrided by RegisterButtonListener
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.button_register),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.button_trial),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

            alertDialog.show();

            Button btRegistration = alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL);
            Button btTrial = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            btRegistration.setOnClickListener(new RegisterButtonListener(alertDialog));
            btTrial.setOnClickListener(new TrialButtonListener(alertDialog));

            TextView tvMessage = (TextView) alertDialog.findViewById(R.id.tvRegMessage);
            tvMessage.setMovementMethod(LinkMovementMethod.getInstance());
            tvMessage.setText(Html.fromHtml(context.getString(R.string.please_register_message)));
        } catch (Exception e) {
            //Handle BadTokenException.
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ExchangesServiceControl.startOrStopServiceIfNeed(this);
    }

    private void initUI() {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

        setContentView(R.layout.activity_main);

        // setup action bar for tabs
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayShowTitleEnabled(false);

        ActionBar.Tab tab = actionBar.newTab()
                .setText(R.string.alarms)
                .setTabListener(new TabListener<AlarmsFragment>(
                        this, "alarms", AlarmsFragment.class));
        actionBar.addTab(tab);

        tab = actionBar.newTab()
                .setText(R.string.settings)
                .setTabListener(new TabListener<SettingsFragment>(
                        this, "settings", SettingsFragment.class));
        actionBar.addTab(tab);
    }

    @Override
    public void onCheckResult(boolean result) {
        if (progressDialog != null)
            progressDialog.dismiss();
        if (result) alertDialog.dismiss();
        else {
            TextView tvMessage = (TextView) alertDialog.findViewById(R.id.tvRegMessage);
            tvMessage.setMovementMethod(LinkMovementMethod.getInstance());
            tvMessage.setText(Html.fromHtml(context.getString(R.string.license_not_valid_message)));
        }
    }



    private class RegisterButtonListener implements View.OnClickListener {
        private final Dialog dialog;

        public RegisterButtonListener(Dialog dialog) {
            this.dialog = dialog;
        }

        @Override
        public void onClick(View v) {
            if (Utils.isOnline(getApplicationContext())) {
                EditText edRegistrationKey = (EditText) alertDialog.findViewById(R.id.edRegKey);
                if (!edRegistrationKey.getText().toString().equals("")) {
                    progressDialog = ProgressDialog.show(context, "", "Checking...", false, true);
                    licenseChecker.activate(edRegistrationKey.getText().toString());
                } else Toast.makeText(getApplicationContext(), getString(R.string.checkYourInput), 3000).show();
            } else Toast.makeText(getApplicationContext(), getString(R.string.internetIsNotAval), 3000).show();
        }
    }

    private class TrialButtonListener implements View.OnClickListener {
        private final Dialog dialog;

        public TrialButtonListener(Dialog dialog) {
            this.dialog = dialog;
        }

        @Override
        public void onClick(View v) {

            if (Utils.isOnline(getApplicationContext())) {
                progressDialog = ProgressDialog.show(context, "", "Checking...", false, true);
                licenseChecker.checkTrial();
            } else Toast.makeText(getApplicationContext(), getString(R.string.internetIsNotAval), 3000).show();

        }
    }

}
