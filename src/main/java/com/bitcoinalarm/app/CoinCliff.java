
package com.bitcoinalarm.app;

import android.app.Application;
import com.bitcoinalarm.app.database.HelperFactory;

public class CoinCliff
        extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        HelperFactory.setHelper(getApplicationContext());
    }



    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        super.onTerminate();
    }
}
