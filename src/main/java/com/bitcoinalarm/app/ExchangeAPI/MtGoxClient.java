package com.bitcoinalarm.app.ExchangeAPI;


import android.util.Log;
import com.bitcoinalarm.app.domens.Alarm;
import com.bitcoinalarm.app.domens.MtGoxResult;
import com.bitcoinalarm.app.interfaces.IApi;
import com.bitcoinalarm.app.interfaces.IDataListener;
import com.bitcoinalarm.app.interfaces.IListenerForResult;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//import sun.misc.BASE64Decoder;
//import sun.misc.BASE64Encoder;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 05.04.13
 * Time: 3:32
 * To change this template use File | Settings | File Templates.
 */
public class MtGoxClient implements IApi, IListenerForResult {

    private final static String TAG = "MtGoxClient";


    private IDataListener dataListener;
    private String key;
    private String secret;

    public static String ExchangeArray[] = {"MtGox"};//, "MtGoxFake"};

    public static String TypeResultArray[] = { "avg", "high", "low", "sell", "buy"};

    public static String CurrencyArray[] = {"USD", "AUD", "CAD", "CHF", "CNY", "DKK", "EUR", "GBP", "HKD", "JPY", "NZD", "PLN", "RUB", "SEK", "SGD", "THB"};

    public static String ReasonArray[] = {"price rises above", "price drops below", "price goes up", "price goes down"};

    public static String ResultArray[] = {"price rose above", "price dropped below", "price went up", "price went down"};

    public static String PeriodTypesArray[] = {"sec", "min", "hour"};

    public static String NotificationTypeArray[] = {"Alarm", "Notification"};

    public static final List<String> CurrencyList;

    public static final List<String> ReasonList;

    static {
        CurrencyList = new ArrayList<String>();
        int i = 0;
        for (String currency : CurrencyArray) {
            CurrencyList.add(currency);
        }

        ReasonList = new ArrayList<String>();
        for (String event : ReasonArray) {
            ReasonList.add(event);
        }
    }



    @Override
    public void afterGetResult(String stringResult) {
        try {
            ArrayList<MtGoxResult> result = parseForValue(stringResult);
            if (result != null)
                dataListener.updateData(result);

        } catch (JSONException e) {
            Log.e(TAG, "Parse error");
            e.printStackTrace();
        }
    }


    public MtGoxClient(IDataListener dataListener, String key, String secret) {
        this.dataListener = dataListener;
        this.key = key;
        this.secret = secret;
    }


    @Override
    public void startGetRate(Alarm alarm, float amount, Long millsOfTime) {

        try {
            remoteURLQuery(this, alarm);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    private ArrayList<MtGoxResult> parseForValue(String result) throws JSONException {
        if (result == null) return null;
        JSONObject jsonObject = new JSONObject(result);

        if (jsonObject.getString("result").equals("success")) {

            JSONObject params = jsonObject.getJSONObject("data");
            ArrayList listMtGoxResults = new ArrayList<MtGoxResult>();

            for (int j = 0; j < MtGoxClient.TypeResultArray.length; j++) {
                //*1000 for compatible with MtGOX
                Long timeNow = System.currentTimeMillis()*1000;
                JSONObject param = params.getJSONObject(MtGoxClient.TypeResultArray[j]);
                listMtGoxResults.add(
                        new MtGoxResult(MtGoxClient.TypeResultArray[j],
                                param.getDouble("value"),
                                param.getString("display"),
                                param.getInt("value_int"),
                                param.getString("display_short"),
                                param.getString("currency"),
                                timeNow));
            }

            return listMtGoxResults;
        }

        return null;
    }


    private void remoteURLQuery(IListenerForResult listenerForResult, Alarm alarm) throws IOException {
        // build URL

        String url_path = null;
        url_path = "http://coincliff.com/api";
        if (alarm.getExchangeID() == 0) {
            url_path = "http://data.mtgox.com/api/2/BTC" + MtGoxClient.CurrencyArray[alarm.getCurrencyID()] + "/money/ticker";
        } else {
            url_path = "http://coincliff.com/api";
        }
        AsyncUrlRequest asyncUrlRequest = new AsyncUrlRequest(listenerForResult);
        asyncUrlRequest.execute(url_path);

    }

    protected String buildQueryString(HashMap<String, String> args) {
        String result = new String();
        for (String hashkey : args.keySet()) {
            if (result.length() > 0) result += '&';
            try {
                result += URLEncoder.encode(hashkey, "UTF-8") + "="
                        + URLEncoder.encode(args.get(hashkey), "UTF-8");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }


}
