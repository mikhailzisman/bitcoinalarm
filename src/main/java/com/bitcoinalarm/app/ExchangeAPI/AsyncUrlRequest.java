package com.bitcoinalarm.app.ExchangeAPI;

import android.os.AsyncTask;
import com.bitcoinalarm.app.interfaces.IListenerForResult;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 06.04.13
 * Time: 1:22
 * To change this template use File | Settings | File Templates.
 */
public class AsyncUrlRequest extends AsyncTask<String, String, String> {

    public static String NETWORK_ERROR = "network error";
    private IListenerForResult listenerForResult;

    public AsyncUrlRequest(IListenerForResult listenerForResult) {
        this.listenerForResult = listenerForResult;
    }

    @Override
    protected String doInBackground(String... strings) {

        String result = null;
        try {
            result = remoteUrlTask(strings);
        } catch (IOException e) {
            e.printStackTrace();
            publishProgress(NETWORK_ERROR);
        }
        publishProgress(result);
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        listenerForResult.afterGetResult(values[0]);
    }

    private String remoteUrlTask(String... strings) throws IOException {


        URL queryUrl = new URL(strings[0]);
        HttpURLConnection connection = (HttpURLConnection) queryUrl.openConnection();

        // read info
        byte buffer[] = new byte[16384];
        int len = connection.getInputStream().read(buffer, 0, 16384);
        return (new String(buffer, 0, len, "UTF-8"));
    }


}
