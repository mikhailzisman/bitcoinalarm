package com.bitcoinalarm.app.Utils;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 22.04.13
 * Time: 21:37
 * To change this template use File | Settings | File Templates.
 */
public class Settings {
    public static String MY_PREF = "MY_PREFEFERENCE";
    public static String refreshRateInUnits = "RefreshRate";
    public static String refreshRateTypeID = "RefreshRateTypeID";
    public static String checkWifi = "CheckWifi";


    public static long minRefreshTimeInSeconds = 30;
    public static double minPrice = 0.01;
    public static String launchTime = "LaunchTime";
}
