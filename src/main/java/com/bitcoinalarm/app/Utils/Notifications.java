package com.bitcoinalarm.app.Utils;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import com.bitcoinalarm.app.Activities.AlarmListActivity;
import com.bitcoinalarm.app.R;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 24.04.13
 * Time: 1:17
 * To change this template use File | Settings | File Templates.
 */
public class Notifications {

    private static Notifications instance;
    private NotificationCompat.Builder mBuilder;
    private NotificationManager mNotificationManager;
    public enum Mode {Working, Processing, Canceled}    ;
    public static Notifications getInstance() {
        if (instance == null)
            return new Notifications();
        else return instance;
    }

    public void setState(Context context, Mode mode) {
        switch (mode) {
            case Processing:
                mBuilder = new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.coincliff_refreshing_android_statusbar_ico)
                        .setContentTitle(context.getString(R.string.notificationProcessingTitle))
                        .setContentText(context.getString(R.string.notificationProcessingMessage));
                startNotification(context);
                break;
            case Working:
                mBuilder = new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.coincliff_android_statusbar_ico)
                        .setContentTitle(context.getString(R.string.notificationTitle))
                        .setContentText(context.getString(R.string.notificationMessage));
                startNotification(context);
                break;
            case Canceled:

                   cancleNotification(context);
                break;
        }

    }

    public void startNotification(Context context) {


// Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, AlarmListActivity.class);

// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(AlarmListActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
        int mId = 1;
        mNotificationManager.notify(mId, mBuilder.build());
    }

    void cancleNotification(Context context) {
        mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(1);
    }




}
