package com.bitcoinalarm.app.Utils;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import com.bitcoinalarm.app.R;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 29.04.13
 * Time: 22:38
 * To change this template use File | Settings | File Templates.
 */
public class SoundService {
    private static SoundService ourInstance = new SoundService();
    private MediaPlayer mMediaPlayer = new MediaPlayer();
    private boolean playingNow = false;
    final int countForAlarms = 15-1;
    final int countForNotifications = 1-1;


    private SoundService() {
    }

    public static SoundService getInstance() {
        return ourInstance;
    }

    public List<String> getSoundList(){
        Field[] fields = R.raw.class.getFields();
        List<String> soundsList = new ArrayList<String>();
        for(Field field:fields){
            int lastPointInString = field.toString().lastIndexOf(".");
            soundsList.add(field.toString().substring(lastPointInString+1));
        }
        return soundsList;
    }
    public int getResourceByID(int index) {
        Field[] fields = R.raw.class.getFields();
        try {
            return fields[index].getInt(fields[index]);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void playSound(Context context, int soundID, int volume,  final boolean looping) {
        if (playingNow) return;

        stopSound();

        try {
            Uri path = Uri.parse("android.resource://com.bitcoinalarm.app/" + getResourceByID(soundID));
            if (mMediaPlayer==null) mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(context, path);
            final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mMediaPlayer.setVolume((float)volume/100, (float)volume/100);
                mMediaPlayer.setLooping(false);
                mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){
                    int count = 0;
                    int maxCount = looping==true?countForAlarms:countForNotifications;
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        if(count < maxCount){
                            count++;
                            mediaPlayer.seekTo(0);
                            mediaPlayer.start();
                        } else {

                            VibratorService.getInstance().stopVibrate();
                        }
                    }});
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }
            playingNow = true;
        } catch (IOException e) {
            e.printStackTrace();
            playingNow = false;
        }
    }

    public void stopSound() {
        if (mMediaPlayer != null)
            mMediaPlayer.stop();
        mMediaPlayer.reset();
        playingNow = false;
    }
}
