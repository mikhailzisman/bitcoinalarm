package com.bitcoinalarm.app.Utils;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import com.bitcoinalarm.app.Activities.AlarmWindowActivity;
import com.bitcoinalarm.app.Activities.NotificationsWindowActivity;
import com.bitcoinalarm.app.ExchangeAPI.MtGoxClient;
import com.bitcoinalarm.app.database.AlarmRepository;
import com.bitcoinalarm.app.database.HelperFactory;
import com.bitcoinalarm.app.domens.Alarm;

import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 17.04.13
 * Time: 14:53
 * To change this template use File | Settings | File Templates.
 */
public class AlarmShow {

    private Context context;

    public AlarmShow(Context context) {
        this.context = context;
    }

    public void showAlarmIfEventHappens(Alarm alarm) {
        try {

            Notifications.getInstance().setState(context, Notifications.Mode.Working);

            if (checkForExecuteAlarm(alarm)) {
                wakeUpScreenAndKeyboard(context);
                showMessage(alarm);
                updateStateOfAlarm(alarm);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void wakeUpScreenAndKeyboard(Context context) {

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
        wakeLock.acquire();

        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock keyguardLock =  keyguardManager.newKeyguardLock("TAG");
        keyguardLock.disableKeyguard();
    }

    private void updateStateOfAlarm(Alarm alarm) {
        switch (alarm.getReasonAlertID()){
            case 0:
            case 1:
                //for rise above and drop down alarms we switch off alarms after first trigger
                alarm.setEnable(false);
                AlarmRepository.getAlarmListModel().updateAlarm(alarm);
                AlarmRepository.getInstance().save();
                break;
        }
    }

    private void showMessage(Alarm alarm) {
        switch (alarm.getTypeOfNotificationID()) {
            case 0:

                Intent dialogIntent = new Intent(context, AlarmWindowActivity.class);
                dialogIntent.putExtra("alarm", alarm);
                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(dialogIntent);
                break;

            case 1:

                Intent NotificationDialogIntent = new Intent(context, NotificationsWindowActivity.class);
                NotificationDialogIntent.putExtra("alarm", alarm);
                NotificationDialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                NotificationDialogIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                NotificationDialogIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                context.startActivity(NotificationDialogIntent);

                break;
        }
    }



    private boolean checkForExecuteAlarm(Alarm alarm) throws SQLException {
        boolean alarmShouldBeLaunch = false;
        if (MtGoxClient.ReasonArray[alarm.getReasonAlertID()].equals("price rises above"))
            alarmShouldBeLaunch = HelperFactory.getHelper().getMtGoxResultDAO().ifIntersectPriceEdgeInPeriod(
                    MtGoxClient.CurrencyArray[alarm.getCurrencyID()],
                    MtGoxClient.TypeResultArray[alarm.getResultTypeID()],
                    alarm.getEdgePrice(),
                    alarm.getPeriodTimeInSecond() * 1000,
                    true);

        if (MtGoxClient.ReasonArray[alarm.getReasonAlertID()].equals("price drops below"))
            alarmShouldBeLaunch = HelperFactory.getHelper().getMtGoxResultDAO().ifIntersectPriceEdgeInPeriod(
                    MtGoxClient.CurrencyArray[alarm.getCurrencyID()],
                    MtGoxClient.TypeResultArray[alarm.getResultTypeID()],
                    alarm.getEdgePrice(),
                    alarm.getPeriodTimeInSecond() * 1000,
                    false);

        if (MtGoxClient.ReasonArray[alarm.getReasonAlertID()].equals("price goes up"))
            alarmShouldBeLaunch = HelperFactory.getHelper().getMtGoxResultDAO().ifGrowingPriceForPeriod(
                    MtGoxClient.CurrencyArray[alarm.getCurrencyID()],
                    MtGoxClient.TypeResultArray[alarm.getResultTypeID()],
                    alarm.getPeriodTimeInSecond() * 1000,
                    alarm.getEdgePrice(),
                    true);

        if (MtGoxClient.ReasonArray[alarm.getReasonAlertID()].equals("price goes down"))
            alarmShouldBeLaunch = HelperFactory.getHelper().getMtGoxResultDAO().ifGrowingPriceForPeriod(
                    MtGoxClient.CurrencyArray[alarm.getCurrencyID()],
                    MtGoxClient.TypeResultArray[alarm.getResultTypeID()],
                    alarm.getPeriodTimeInSecond() * 1000,
                    alarm.getEdgePrice(),
                    false);

        return alarmShouldBeLaunch;
    }


}
