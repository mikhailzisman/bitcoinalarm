package com.bitcoinalarm.app.Utils;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.Build;
import com.bitcoinalarm.app.ExchangeAPI.AsyncUrlRequest;
import com.bitcoinalarm.app.R;
import com.bitcoinalarm.app.database.HelperFactory;
import com.bitcoinalarm.app.domens.License;
import com.bitcoinalarm.app.fragments.SettingsFragment;
import com.bitcoinalarm.app.interfaces.ICheckerLicenser;
import com.bitcoinalarm.app.interfaces.IListenerForResult;
import com.licel.stringer.annotations.secured;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 04.05.13
 * Time: 4:12
 * To change this template use File | Settings | File Templates.
 */
@secured
public class LicenseChecker implements IListenerForResult {

    private static final long DAY = 24 * 60 * 60 * 1000;
    private boolean isCorrectLicenseHashCode = false;
    private Context context;
    private String mActivationCode;
    private boolean isCorrectTrialHashDCode = false;
    private ICheckerLicenser checkerLicenser;
    private SettingsFragment listeningFragment;

    public LicenseChecker(Object listener) {
        if (listener instanceof Context)
            this.context = (Context)listener;

        if (listener instanceof SettingsFragment)
            this.context = ((SettingsFragment) listener).getActivity();

        this.checkerLicenser = (ICheckerLicenser) listener;
    }

    public String getUniqueDeviceString() {
        String m_szLongID = m_szDevIDShort();// + m_szBTMAC();
        // compute md5
        MessageDigest m = null;
        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        m.update(m_szLongID.getBytes(), 0, m_szLongID.length());
        // get md5 bytes
        byte p_md5Data[] = m.digest();
        // create a hex string
        String m_szUniqueID = new String();
        for (int i = 0; i < p_md5Data.length; i++) {
            int b = (0xFF & p_md5Data[i]);
            // if it is a single digit, make sure it have 0 in front (proper padding)
            if (b <= 0xF) m_szUniqueID += "0";
            // add number to string
            m_szUniqueID += Integer.toHexString(b);
        }
        // hex string to uppercase
        return m_szUniqueID.toLowerCase();
        //return String.valueOf(8);           //it for test
    }

    public String m_szDevIDShort() {
        return "35" + //we make this look like a valid IMEI
                Build.BOARD.length() % 10 + Build.BRAND.length() % 10 +
                Build.CPU_ABI.length() % 10 + Build.DEVICE.length() % 10 +
                Build.DISPLAY.length() % 10 + Build.HOST.length() % 10 +
                Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 +
                Build.MODEL.length() % 10 + Build.PRODUCT.length() % 10 +
                Build.TAGS.length() % 10 + Build.TYPE.length() % 10 +
                Build.USER.length() % 10; //13 digits
    }

    public void activate(String activationCode) {
        this.mActivationCode = activationCode.toLowerCase().replace("-", "");
        new AsyncUrlRequest(this).execute("http://coincliff.com/activations/" + mActivationCode + "/1");
    }

    public void checkTrial() {
        new AsyncUrlRequest(this).execute("http://coincliff.com/trials/" + getUniqueDeviceString() + "/1");
    }

    public String getHash(String str) {

        return new String(Hex.encodeHex(DigestUtils.md5(str)));
    }

    private String m_szBTMAC() {
        BluetoothAdapter m_BluetoothAdapter = null; // Local Bluetooth adapter
        m_BluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return m_BluetoothAdapter.getAddress();
    }

    @Override
    public void afterGetResult(String result) {

        if (checkNewtworkResult(result)) return;

        if (mActivationCode != null)
            isCorrectLicenseHashCode = getHash(mActivationCode.replace("-", "").toLowerCase()
                    + context.getString(R.string.secret))
                    .equals(result);

        String deviceHash = getHash(getUniqueDeviceString().toLowerCase()
                + context.getString(R.string.secret));

        isCorrectTrialHashDCode = deviceHash.equals(result);
        if (checkerLicenser != null)
            checkerLicenser.onCheckResult(isCorrectTrialHashDCode || isCorrectLicenseHashCode);

        saveLicenseToDatabase(deviceHash);
    }

    private boolean checkNewtworkResult(String result) {
        if ((result == null) || (result.equals(AsyncUrlRequest.NETWORK_ERROR))) {
            if (checkerLicenser != null)
                checkerLicenser.onCheckResult(false);
            return true;
        }
        return false;
    }

    private void saveLicenseToDatabase(String deviceHash) {

        if (isCorrectLicenseHashCode) saveToDatabase(License.REAL_LICENSE, deviceHash);

        if (isCorrectTrialHashDCode) saveToDatabase(License.TRIAL_LICENSE, deviceHash);
    }

    private void saveToDatabase(int licenseType, String hash) {
        try {
            HelperFactory.getHelper().getLicenseDAO().create(new License(hash, licenseType, System.currentTimeMillis()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isLicensedByRealLicense() {

        License lastRealLicense = null;
        try {
            lastRealLicense = HelperFactory.getHelper().getLicenseDAO().getLastActiveLicense(License.REAL_LICENSE);
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (lastRealLicense != null)
            return true;

        return false;
    }

    public boolean isLicenedByTrial() {

        License lastTrialLicense = null;
        try {
            lastTrialLicense = HelperFactory.getHelper().getLicenseDAO().getLastActiveLicense(License.TRIAL_LICENSE);
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


        if (lastTrialLicense != null)
            if (deivceHashEqualResultHash(lastTrialLicense)
                    && trialLicenceIsActually(lastTrialLicense)) {
                return true;
            }

        return false;
    }

    public boolean isRegistredFromDatabase() {
        return isLicenedByTrial() || isLicensedByRealLicense();
    }

    private boolean deivceHashEqualResultHash(License license) {
        return getHash(getUniqueDeviceString().toLowerCase()
                + context.getString(R.string.secret))
                .equals(license.getLicense().toLowerCase());
    }

    private boolean trialLicenceIsActually(License lastTrialLicense) {
        return ((System.currentTimeMillis() - lastTrialLicense.getTimeOfRegistration() <= DAY));
    }
}
