package com.bitcoinalarm.app.Utils;

import android.content.Context;
import android.os.Vibrator;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 29.04.13
 * Time: 21:48
 * To change this template use File | Settings | File Templates.
 */
public class VibratorService {
    private static VibratorService ourInstance = new VibratorService();
    private boolean vibrateNow = false;

    public static VibratorService getInstance() {
        return ourInstance;
    }


    // Get instance of VibratorService from current Context
    Vibrator mVibrator;

    long[] patternRepeat = { 0, 200, 700 };


// The "0" means to repeat the patternRepeat starting at the beginning and -1 means do not stop
// You will vibrate for your pause times and pause for your vibrate times !
    
    private  VibratorService() {

    }
    
    public void startVibrate(Context context, boolean repeat){
        if (mVibrator== null)
            mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (!mVibrator.hasVibrator()) return;
        if ((!vibrateNow)&&(mVibrator!=null))
            mVibrator.vibrate(patternRepeat, repeat==true?0:-1);
        vibrateNow = true;
    }

    public void stopVibrate(){
        if (mVibrator!=null)
            mVibrator.cancel();

        vibrateNow = false;
    }

}
