package com.bitcoinalarm.app.Utils;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 22.04.13
 * Time: 21:35
 * To change this template use File | Settings | File Templates.
 */
public class SharedPreferenceSingleton {
    private static SharedPreferenceSingleton ourInstance = new SharedPreferenceSingleton();

    public static SharedPreferenceSingleton getInstance() {
        return ourInstance;
    }

    private SharedPreferenceSingleton() {
    }
}
