package com.bitcoinalarm.app.interfaces;

import java.sql.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 05.04.13
 * Time: 2:13
 * To change this template use File | Settings | File Templates.
 */
public interface IAlarmListModel {
    IDisplayAlarm getNextDisplayAlarm(Date curTime);

    IDisplayAlarm createAlarm();

    void updateAlarm(IDisplayAlarm iDisplayAlarm);

    void addAlarm(IDisplayAlarm iDisplayAlarm);

    void deleteAlarm(IDisplayAlarm item);

    void takeAlarmForEdit(int alarmID);

    IDisplayAlarm getEditingAlarm();

    void saveEditingAlarm(boolean stayEditing);

    void resetEditingAlarm();

}
