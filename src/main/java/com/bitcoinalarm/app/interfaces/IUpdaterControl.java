package com.bitcoinalarm.app.interfaces;

import android.content.Context;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 16.04.13
 * Time: 22:29
 * To change this template use File | Settings | File Templates.
 */
public interface IUpdaterControl {

    void startUpdater(Context context);

    void stopUpdater(Context context);
}
