package com.bitcoinalarm.app.interfaces;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 05.04.13
 * Time: 2:23
 * To change this template use File | Settings | File Templates.
 */
public interface IAlarmRepository {

    public abstract void load();

    public abstract void save();
}
