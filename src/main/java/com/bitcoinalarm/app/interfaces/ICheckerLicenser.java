package com.bitcoinalarm.app.interfaces;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 06.05.13
 * Time: 14:59
 * To change this template use File | Settings | File Templates.
 */
public interface ICheckerLicenser {
    public void onCheckResult(boolean result);
}
