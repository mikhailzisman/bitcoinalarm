package com.bitcoinalarm.app.interfaces;

import com.bitcoinalarm.app.domens.EntityState;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 05.04.13
 * Time: 2:13
 * To change this template use File | Settings | File Templates.
 */
public interface IDisplayAlarm {

    public int getId();


    public EntityState getState();

    public void setState(EntityState state);

    public boolean isEnable();

    public void setEnable(boolean enable);

    public void setId(int id);


    public int getExchangeID();

    public void setExchangeID(int exchangeID);

    public void setReasonAlertID(int id);

    public void setEdgePrice(double edgePrice);

    public void setPeriodTimeInUnit(int periodTime);

    void setCurrencyID(int selectedItemPosition);
}
