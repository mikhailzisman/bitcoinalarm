package com.bitcoinalarm.app.interfaces;

import com.bitcoinalarm.app.domens.MtGoxResult;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 06.04.13
 * Time: 14:28
 * To change this template use File | Settings | File Templates.
 */
public interface IDataListener {

    void updateData(ArrayList<MtGoxResult> result);
}
