package com.bitcoinalarm.app.interfaces;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 06.05.13
 * Time: 11:24
 * To change this template use File | Settings | File Templates.
 */
public interface LicenseCheckerCallback {

    void allow(int policyReason);

    void dontAllow(int policyReason);

    void applicationError(int errorCode);

}
