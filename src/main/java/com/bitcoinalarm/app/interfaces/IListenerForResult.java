package com.bitcoinalarm.app.interfaces;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 06.04.13
 * Time: 1:18
 * To change this template use File | Settings | File Templates.
 */
public interface IListenerForResult {
    void afterGetResult(String result);
}
