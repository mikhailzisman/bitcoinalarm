package com.bitcoinalarm.app.interfaces;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 05.04.13
 * Time: 2:19
 * To change this template use File | Settings | File Templates.
 */
public interface IListModelData<T> {
    ArrayList<T> getItemList();

    ArrayList<T> getDeletedItemList();

}