package com.bitcoinalarm.app.interfaces;

import android.content.Context;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 16.04.13
 * Time: 23:16
 * To change this template use File | Settings | File Templates.
 */
public interface IListenerRepositoryChanged {
    void onAlarmStopped(Context context);

    void onAlarmStarted(Context context);

    void onAlarmUpdated(Context context);
}
